//
//  ApiServiceError.swift
//  Movie
//
//  Created by Тимур Чеберда on 30.03.2020.
//  Copyright © 2020 Тимур Чеберда. All rights reserved.
//

import Foundation

enum ApiServiceError: Error {
    case badRequest
    case forbidden
    case cantSerialize
    case general
    case internalServer
    case responseDataNil
    case unknownNetwork
}
