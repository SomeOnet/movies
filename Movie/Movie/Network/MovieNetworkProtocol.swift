//
//  MovieNetworkProtocol.swift
//  Movie
//
//  Created by Тимур Чеберда on 30.03.2020.
//  Copyright © 2020 Тимур Чеберда. All rights reserved.
//

import Foundation

protocol MovieNetworkProtocol {
    func getAllMovie(
        completionHandler: @escaping (MovieResponse) -> (),
        errorHandler: @escaping (ApiServiceError) -> ()
    )
}
