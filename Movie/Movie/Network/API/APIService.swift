//
//  APIService.swift
//  Movie
//
//  Created by Тимур Чеберда on 30.03.2020.
//  Copyright © 2020 Тимур Чеберда. All rights reserved.
//

import Foundation
import Alamofire

struct APIService: MovieNetworkProtocol {
    
    private let decoder = JSONDecoder()
    private let baseURL = "https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=70975a712a50f1825d37028d9a9d58fb"
    
    init() {
        decoder.keyDecodingStrategy = .convertFromSnakeCase
    }
    
    func getAllMovie(
        completionHandler onSuccess: @escaping (MovieResponse) -> (),
        errorHandler onFailure: @escaping (ApiServiceError) -> ()
        ) {
        AF.request(baseURL)
            .validate()
            .response { response in
                switch response.result {
                case .success:
                    guard let data = response.data
                        else {
                            onFailure(.responseDataNil)
                            return
                    }
                    guard
                        let model = try? self.decoder.decode(MovieResponse.self, from: data)
                        else {
                            onFailure(.cantSerialize)
                            return
                    }
                    
                    onSuccess(model)
                    
                case .failure:
                    guard
                        let httpError = response.response?.statusCode
                        else {
                            onFailure (.general)
                            return
                    }
                    switch httpError {
                    case 300..<400: onFailure(.badRequest)
                    case 400..<500: onFailure(.forbidden)
                    case 500..<600: onFailure(.internalServer)
                    default:
                        onFailure(.unknownNetwork)
                    }
                }
        }
    }
}
