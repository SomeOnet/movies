//
//  MovieListInteractorProtocols.swift
//  Movie
//
//  Created by Тимур Чеберда on 30.03.2020.
//  Copyright © 2020 Тимур Чеберда. All rights reserved.
//

import Foundation

protocol MovieListInteractorInput: class {
    func requestMovieList()
}

protocol MovieListInteractorOutput: class {
    func set(response: MovieResponse)
    func setNoContentError()
    func setInternetError()
}
