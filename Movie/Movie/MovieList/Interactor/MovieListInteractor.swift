//
//  MovieListInteractor.swift
//  Movie
//
//  Created by Тимур Чеберда on 30.03.2020.
//  Copyright © 2020 Тимур Чеберда. All rights reserved.
//

import Foundation
import Alamofire

final class MovieListInteractor {

    weak var output: MovieListInteractorOutput?
    
    private let apiService: MovieNetworkProtocol
    
    init(apiService: MovieNetworkProtocol) {
        self.apiService = apiService
    }
}

extension MovieListInteractor: MovieListInteractorInput {
    
    func requestMovieList() {
        apiService.getAllMovie(completionHandler: { [weak self] in
            self?.output?.set(response: $0)
        }) { [weak self] error in
            print("\(error)")
        }
    }
}
