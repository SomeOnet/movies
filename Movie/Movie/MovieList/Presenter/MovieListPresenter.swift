//
//  MovieListPresenter.swift
//  Movie
//
//  Created by Тимур Чеберда on 30.03.2020.
//  Copyright © 2020 Тимур Чеберда. All rights reserved.
//

import Foundation

final class MovieListPresenter {
   
    weak var view: MovieListViewInput?
    private let interactor: MovieListInteractorInput
    
    init(interactor: MovieListInteractorInput) {
        self.interactor = interactor
    }
}

extension MovieListPresenter: MovieListViewOutput {
    func viewLoaded() {
        interactor.requestMovieList()
        view?.set(state: .loading)
    }
}

extension MovieListPresenter: MovieListInteractorOutput {
    func set(response: MovieResponse) {
        view?.set(state: .hasData(
            response.results.map { self.convert(movies: $0) }
            )
        )
    }
    
    func setNoContentError() {
        
    }
    
    func setInternetError() {
        
    }
    
    private func convert(movies: MovieResponse.MovieResult) -> MovieListCollectionCell.ViewModel {
        return MovieListCollectionCell.ViewModel(
            title: movies.originalTitle,
            image: movies.posterPath,
            rating: "\(movies.voteAverage)"
        )
    }
}
//    func viewLoaded() {
//        interactor.requestData()
//    }
//
//    func setData(movieResponse: MovieResponse) {
//        view?.setViewModel(viewModel: movieResponse)
//        print(movieResponse.results)
//    }
//
//    private func convert(movies: MovieResponse.MovieResult) -> MovieListCollectionCell.ViewModel {
//
//        return MovieListCollectionCell.ViewModel(
//            title: movies.originalTitle,
//            image: movies.posterPath,
//            rating: "\(movies.voteAverage)"
//        )
//    }
