//
//  MovieListAssembly.swift
//  Movie
//
//  Created by Тимур Чеберда on 30.03.2020.
//  Copyright © 2020 Тимур Чеберда. All rights reserved.
//

import Foundation

final class MovieListAssembly {
    
    static func assembly(view: MovieListView) {
        
        let apiService = APIService()
        let interactor = MovieListInteractor(apiService: apiService)
        let presenter = MovieListPresenter(interactor: interactor)
        
        view.output = presenter
        
        presenter.view = view
        
        interactor.output = presenter
    }
}

