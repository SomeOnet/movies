//
//  MovieListCollectionCell.swift
//  Movie
//
//  Created by Тимур Чеберда on 30.03.2020.
//  Copyright © 2020 Тимур Чеберда. All rights reserved.
//

import Foundation
import SnapKit
import UIKit
import Kingfisher

class MovieListCollectionCell: UICollectionViewCell {
           
    private let posterImageView = UIImageView()
    private let ratingContainerView = UIView()
    
    private let ratingLabel = UILabel()
    private let titleLabel = UILabel()
    private let genresLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        embedViews()
        setupLayout()
        setupAppearance()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func embedViews() {
        [ posterImageView,
          ratingContainerView,
          ratingLabel,
          titleLabel
        ].forEach { contentView.addSubview($0) }
        ratingContainerView.addSubview(genresLabel)
    }
    
    //MARK:- Setup Layout
    private func setupLayout() {
        posterImageView.snp.makeConstraints { (make) in
            make.top.equalTo(contentView.snp.top)
            make.leading.equalTo(contentView.snp.leading)
            make.trailing.equalTo(contentView.snp.trailing)
            make.bottom.equalTo(titleLabel.snp.top).offset(-8)
        }
        
        ratingContainerView.snp.makeConstraints { (make) in
            make.top.equalTo(posterImageView.snp.top).offset(12)
            make.leading.equalTo(posterImageView.snp.leading).offset(-8)
            make.width.equalTo(60)
            make.height.equalTo(24)
        }
        
        ratingLabel.snp.makeConstraints { (make) in
            make.center.equalTo(ratingContainerView)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(posterImageView.snp.bottom).offset(8)
            make.leading.equalTo(posterImageView.snp.leading)
            make.trailing.equalTo(posterImageView.snp.trailing)
            make.bottom.equalTo(genresLabel.snp.top).offset(-4)
        }

        genresLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(4)
            make.leading.equalTo(titleLabel.snp.leading)
            make.bottom.equalTo(contentView.snp.bottom)
        }
    }

    //MARK:- Appearance
    private func setupAppearance() {
        setupLabels()
        setupView()
    }
    
    private func setupLabels() {
        ratingLabel.textColor = UIColor.white
        ratingLabel.font = UIFont.systemFont(ofSize: 18, weight: .medium)
                
        titleLabel.textColor = UIColor.white
        titleLabel.font = UIFont.systemFont(ofSize: 18, weight: .regular)
        titleLabel.textAlignment = .left
        titleLabel.numberOfLines = 0
        
        genresLabel.textColor = UIColor.darkGray
        genresLabel.font = UIFont.systemFont(ofSize: 18, weight: .regular)
        genresLabel.textAlignment = .left
    }
    
    private func setupView() {
        [posterImageView,
         ratingContainerView
        ].forEach { $0.clipsToBounds = true }
        
        posterImageView.contentMode = .scaleAspectFill
        posterImageView.layer.cornerRadius = 8
        posterImageView.kf.indicatorType = .activity
        
        ratingContainerView.layer.cornerRadius = 4
        ratingContainerView.backgroundColor = UIColor.systemBlue
    }
}

extension MovieListCollectionCell {
    // MARK: - View Model
    struct ViewModel {
        let movieBaseUrlPoster = "https://image.tmdb.org/t/p/w185"
        let title: String
        let image: String
        let rating: String
//        let genre: [Int]
    }
    
    func set(viewModel: ViewModel) {
        titleLabel.text = viewModel.title
        ratingLabel.text = viewModel.rating
        ratingLabel.text = viewModel.rating
//        movieGenre.text = viewModel.genre
        posterImageView.kf.setImage(with: URL(string: "\(viewModel.movieBaseUrlPoster)\(viewModel.image)"))
    }
}
