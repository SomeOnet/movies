//
//  ViewController.swift
//  Movie
//
//  Created by Тимур Чеберда on 30.03.2020.
//  Copyright © 2020 Тимур Чеберда. All rights reserved.
//

import UIKit
import AudioToolbox.AudioServices

class ViewController: UIViewController, MovieListView {
    
    var output: MovieListViewOutput?
        
    private let collectionView = UICollectionView(
        frame: .zero,
        collectionViewLayout: ScrollingCollectionViewFlowLayout()
    )
    
    private var viewModel: [MovieListCollectionCell.ViewModel] = []
    
    private let baseRectangleWidth: CGFloat = 264
    private let baseLayoutCellWidth: CGFloat = 375
        
    private var screenSize: CGSize {
        return UIScreen.main.bounds.size
    }
    
    private let tactileVibrations = SystemSoundID(1520)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Hello"
        view.backgroundColor = .white
        
        embedViews()
        setupLayout()
        setupBehaviour()
        setupAppearance()        
        output?.viewLoaded()
    }
    
    private func embedViews() {
        view.addSubview(collectionView)
    }
    
    private func setupLayout() {
        collectionView.snp.makeConstraints { (make) in
            make.top.equalTo(view.snp.top)
            make.leading.equalTo(view.snp.leading)
            make.trailing.equalTo(view.snp.trailing)
            make.bottom.equalTo(view.snp.bottom)
        }
    }
    
    private func setupBehaviour() {
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.isPagingEnabled = false
        collectionView.decelerationRate = .fast
        
        collectionView.register(
            MovieListCollectionCell.self,
            forCellWithReuseIdentifier: .cellIdentifier)
    }
    
    // MARK:- Appearance
    private func setupAppearance() {
        let ratioWidth: CGFloat = screenSize.width / baseLayoutCellWidth
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = UIColor.systemIndigo
        collectionView.contentInset = UIEdgeInsets(
            top: 0.0,
            left: 55.0 * ratioWidth,
            bottom: 0.0,
            right: 55.0 * ratioWidth
        )
        
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.minimumLineSpacing = 16
        layout.scrollDirection = .horizontal
    }
    
//    private func loadData() {
//        apiService.getAllMovie(completionHandler: { [weak self] (model) in
//            self?.viewModels = model.results.compactMap { self?.convert(movies: $0) }
////            self?.tableView.reloadData()
//            self?.collectionView.reloadData()
//            print(model.totalPages)
//        }) { (error) in
//            print(error)
//        }
//    }
}

extension ViewController: MovieListViewInput {
    func set(state: MovieListViewState) {
        switch state {
        case .hasData(let viewModel):
            handleHasDataState(viewModel: viewModel)
        case .error:
            print("Error")
        case .loading:
            print("Loading")
        }
    }
    
    private func handleHasDataState(
        viewModel: [MovieListCollectionCell.ViewModel]
    ) {
        self.viewModel = viewModel
        collectionView.reloadData()
    }
}

extension ViewController: UICollectionViewDataSource {
    
    public func collectionView(
        _ collectionView: UICollectionView,
        numberOfItemsInSection section: Int
    ) -> Int {
        return viewModel.count
    }
    
    public func collectionView(
        _ collectionView: UICollectionView,
        cellForItemAt indexPath: IndexPath
    ) -> UICollectionViewCell {
        
        let cellViewModel = viewModel[indexPath.item]
        let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: .cellIdentifier,
            for: indexPath
            ) as! MovieListCollectionCell
        cell.set(viewModel: cellViewModel)
                
        return cell
    }
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
        
    func collectionView(
        _ collectionView: UICollectionView,
        didSelectItemAt indexPath: IndexPath
    ) {
//        viewModel[indexPath.row].onClick()
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        
        let ratioWidth: CGFloat = screenSize.width / baseLayoutCellWidth
        let resultWidth = baseRectangleWidth * ratioWidth
        
        let resultHeight = resultWidth / 33 * 59
        return .init(width: resultWidth, height: resultHeight)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        AudioServicesPlaySystemSound(tactileVibrations)
    }
}

private extension String {
    static let cellIdentifier = "cell"
}
