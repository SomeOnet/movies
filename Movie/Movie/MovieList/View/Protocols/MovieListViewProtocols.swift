//
//  MovieListViewProtocols.swift
//  Movie
//
//  Created by Тимур Чеберда on 30.03.2020.
//  Copyright © 2020 Тимур Чеберда. All rights reserved.
//

import Foundation

protocol MovieListViewInput: class {
    func set(state: MovieListViewState)
}

protocol MovieListViewOutput: class {
    func viewLoaded()
}

protocol MovieListView: MovieListViewInput {
    var output: MovieListViewOutput? { get set }
}

enum MovieListViewState {
    case hasData([MovieListCollectionCell.ViewModel])
    case error
    case loading
}
