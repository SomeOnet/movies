//
//  MovieGenre.swift
//  Movie
//
//  Created by Тимур Чеберда on 30.03.2020.
//  Copyright © 2020 Тимур Чеберда. All rights reserved.
//

import Foundation

enum MovieImage {
    case placeHolder
}

enum MovieGenre: Int {
    case adventure = 12
    case animation = 16
    case action = 28
    case crime = 80
    case comedy = 35
    case drama = 18
    case science = 878
    case thriller = 53
    case war = 10752
    case western = 37
    
    var genre: String {
        switch self {
        case .adventure: return "Adventure"
        case .animation: return "Animation"
        case .action: return "Action"
        case .crime: return "Crime"
        case .comedy: return "Comedy"
        case .drama: return "Drama"
        case .science: return "Science Fiction"
        case .thriller: return "Thriller"
        case .war: return "War"
        case .western: return "Western"
        }
    }
}
