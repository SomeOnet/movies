//
//  MovieResponse.swift
//  Movie
//
//  Created by Тимур Чеберда on 30.03.2020.
//  Copyright © 2020 Тимур Чеберда. All rights reserved.
//

import Foundation

struct MovieResponse: Codable {
    let page: Int
    let totalResults: Int
    let totalPages: Int
    let results: [MovieResult]
}

extension MovieResponse {
    struct MovieResult: Codable {
        let voteAverage: Double
        let title: String
        let posterPath: String
        let originalTitle: String
        let backdropPath: String
        let overview: String
        let genreIds: [Int]
        let releaseDate: String
        
//        public var ratingText: String {
//            let rating = Int(voteAverage)
//            let ratingText = (0..<rating).reduce("") { (rate, _) -> String in
//                return rate + "⭐️"
//            }
//            return ratingText
//        }
    }
}
